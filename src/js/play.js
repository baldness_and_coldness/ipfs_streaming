App = {
  web3Provider: null,
  contracts: {},

  //初始化视频
  init: function () {
    var video = document.getElementById('video');
    var playlist = new URLSearchParams(window.location.search).get("video")
    App.show(playlist)
    if (Hls.isSupported()) {
      var hls = new Hls();
      hls.loadSource(playlist);
      hls.attachMedia(video);
      hls.on(Hls.Events.MANIFEST_PARSED, function () {
        video.play();
      })
    }
    return App.initWeb3();
  },

  //展示视频名称
  show: function (name){
    var title = $('#title');
    var str = name.slice(0,-5)
    title.find(".show").text(str)
    return true;
  }
};

//初始化
$(function () {
  $(window).load(function () {
    App.init();
  });
});
