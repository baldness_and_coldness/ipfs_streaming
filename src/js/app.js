App = {
  web3Provider: null,
  contracts: {},

  init: function () {
    $.getJSON("/playlists").then(function (files) {
      for (i = 0; i < files.length; i++) {
        playlist = files[i]
        var playlistsRow = $('#playlistsRow');
        var playlistsTemplate = $('#playlistsTemplate');
        playlistsTemplate.find('.link').attr('href', '/play.html?video='+playlist);
        playlistsTemplate.find('.link').text(playlist.slice(0,-5))
        playlistsRow.append(playlistsTemplate.html());
      }
    })
    App.bindEvents();
  },

  bindEvents: function () {
    $(document).on('click', '.btn-pay', App.handleAdopt);
    $(document).on('click', '.btn-submit', App.handleClickUpload);
  },

  handleClickUpload: function () {
    document.getElementById('loader').style = "visibility:visible;"
  }
};

$(function () {
  $(window).load(function () {
    App.init();
  });
});
