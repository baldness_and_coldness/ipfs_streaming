IPFS在线视频项目：

项目介绍：https://blog.csdn.net/awsl_6699/article/details/123505190

安装：
- 安装 ffmpeg（参考https://blog.csdn.net/chy466071353/article/details/54949221）
- 安装 ipfs （参考https://blog.csdn.net/lhx0525/article/details/103528680）
- 初始化ipfs并运行ipfs daemon命令启动守护进程 
- npm install安装相关包 
- npm start 开始项目

浏览器网址：http://localhost:3000/

——————————————————————————————————————————————————

Video streaming on IPFS  project: 

Installation: 
- Install ffmpeg
- Install ipfs，Init & run daemon 
- npm install 
- npm start

website:http://localhost:3000/
